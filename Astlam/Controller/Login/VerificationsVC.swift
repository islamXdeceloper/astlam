//
//  VerificationsVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/5/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import StatusAlert

class VerificationsVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var TxtOPT1: UITextFieldX!
    @IBOutlet weak var TxtOPT2: UITextFieldX!
    @IBOutlet weak var TxtOPT3: UITextFieldX!
    @IBOutlet weak var TxtOPT4: UITextFieldX!
    @IBOutlet weak var TxtOPT5: UITextFieldX!
    
    
    @IBOutlet weak var checkMark: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        TxtOPT1.delegate = self
        TxtOPT2.delegate = self
        TxtOPT3.delegate = self
        TxtOPT4.delegate = self
        TxtOPT5.delegate = self
              
              
        TxtOPT1.becomeFirstResponder()
              
        // Do any additional setup after loading the view.
    }
    
    @IBAction func NoResponseMe(_ sender: Any) {
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text!.count < 1) && (string.count > 0 ) {
            if textField == TxtOPT1 {
                TxtOPT2.becomeFirstResponder()
            }
            if textField == TxtOPT2 {
                TxtOPT3.becomeFirstResponder()
            }
            if textField == TxtOPT3 {
                TxtOPT4.becomeFirstResponder()
            }
            if textField == TxtOPT4 {
                TxtOPT5.becomeFirstResponder()
            }
            if textField == TxtOPT5 {
                TxtOPT5.becomeFirstResponder()
            }
          
            
            textField.text = string
            
            return false
        } else if (textField.text!.count >= 1) && (string.count == 0 ) {
            
            if textField == TxtOPT2 {
                TxtOPT1.becomeFirstResponder()
            }
            if textField == TxtOPT3 {
                TxtOPT2.becomeFirstResponder()
            }
            if textField == TxtOPT4 {
                TxtOPT3.becomeFirstResponder()
            }
            if textField == TxtOPT5 {
                TxtOPT4.becomeFirstResponder()
            }
            if textField == TxtOPT1 {
                TxtOPT1.resignFirstResponder()
            }
            textField.text = ""
            return false
            
        } else if textField.text!.count >= 1 {
            textField.text = string
            return false
        }
        
        return true
    }

    
    
    @IBAction func loginTapped(_ sender: Any) {
        view.endEditing(true)
        let statusAlert = StatusAlert()
        guard let one = TxtOPT1.text,!one.isEmpty else {
            statusAlert.image = UIImage(named: "error")
            statusAlert.title = "Dont let Code Empty"
            statusAlert.showInKeyWindow()
            return
        }
        guard let two = TxtOPT2.text,!two.isEmpty else {
            statusAlert.image = UIImage(named: "error")
            statusAlert.title = "Dont let Code Empty"
            statusAlert.showInKeyWindow()
            return
        }
        guard let three = TxtOPT3.text,!three.isEmpty else {
            statusAlert.image = UIImage(named: "error")
            statusAlert.title = "Dont let Code Empty"
            statusAlert.showInKeyWindow()
            return
        }
        guard let four = TxtOPT4.text,!four.isEmpty else {
            statusAlert.image = UIImage(named: "error")
            statusAlert.title = "Dont let Code Empty"
            statusAlert.showInKeyWindow()
                   return
               }
        guard let five = TxtOPT5.text,!five.isEmpty else {
            statusAlert.image = UIImage(named: "error")
            statusAlert.title = "Dont let Code Empty"
            statusAlert.showInKeyWindow()
                   return
               }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MandopVC")
           vc.modalPresentationStyle = .fullScreen
           vc.modalTransitionStyle = .crossDissolve
           show(vc, sender: self)
    }
    


}
