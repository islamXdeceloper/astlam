//  ViewController.swift
//  Astlam
//  Created by Eslam Ahmed on 6/4/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import StatusAlert

class LoginControllerVC: UIViewController {

    @IBOutlet weak var phoneNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
   
        }    

    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func tappedVerify(_ sender: Any) {
        view.endEditing(true)
        guard let phone = phoneNumber.text, !phone.isEmpty else {
            let statusAlert = StatusAlert()
             statusAlert.image = UIImage(named: "error")
             statusAlert.title = "Phone Number Requierd"
             statusAlert.showInKeyWindow()
            return
        }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "VerificationsVC")
           vc.modalPresentationStyle = .fullScreen
           vc.modalTransitionStyle = .crossDissolve
           show(vc, sender: self)
    }
    
}

