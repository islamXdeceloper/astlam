//
//  OrderListVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/8/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class OrderListVC: BaseViewController {

    @IBOutlet weak var descriptionOrder: UITextView!
    
    @IBOutlet weak var descriptionLocation: UITextView!
    
    @IBOutlet weak var sendToAll: UIButtonX!
    
    @IBOutlet weak var cancleBtn: UIButtonX!
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()

        // Do any additional setup after loading the view.
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
           self.descriptionOrder.layer.borderColor = borderGray.cgColor
           self.descriptionOrder.layer.borderWidth = 1
           self.descriptionOrder.layer.cornerRadius = 2
        
        self.descriptionLocation.layer.borderColor = borderGray.cgColor
        self.descriptionLocation.layer.borderWidth = 1
        self.descriptionLocation.layer.cornerRadius = 2
        
        
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OrderPriceVC")
          vc.modalPresentationStyle = .fullScreen
          vc.modalTransitionStyle = .crossDissolve
          show(vc, sender: self)
        
    }
    
    
    @IBAction func CancleTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    

}
