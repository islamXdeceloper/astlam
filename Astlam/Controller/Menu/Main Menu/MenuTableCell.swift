//
//  MenuTableCell.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/8/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class MenuTableCell: UITableViewCell {
    @IBOutlet weak var labelFetched: UILabel!
    @IBOutlet weak var imageHeader: UIImageView!
    
    @IBOutlet weak var imageLeft: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
