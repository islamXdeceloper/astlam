////
////  MenuTableVC.swift
////  Astlam
////
////  Created by Eslam Ahmed on 6/8/20.
////  Copyright © 2020 Eslam Ahmed. All rights reserved.
////
//
//import UIKit
//
//
//
//
//
//class MenuTableVC: UIViewController {
//
//    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var MyOrderView: UIView!
//    @IBOutlet weak var MyAccountView: UIView!
//    @IBOutlet weak var myCoponView: UIView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableView.layer.cornerRadius = 15
//        view.bringSubviewToFront(tableView)
//        tableView.tableFooterView = UIView()
//    }
//    
//
//   
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//        self.MyAccountView.isHidden = true
//        self.MyOrderView.isHidden = true
//        self.myCoponView.isHidden = true
//
//    }
//
//}
//
//extension MenuTableVC: UITableViewDataSource,UITableViewDelegate {
//    // MARK: - Table view data source
//
//        func numberOfSections(in tableView: UITableView) -> Int {
//           // #warning Incomplete implementation, return the number of sections
//        return 1
//       }
//
//        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//           // #warning Incomplete implementation, return the number of rows
//            return data.count
//       }
//
//       
//        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//   
//            if indexPath.row == 0 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
//                          return cell
//            }
//            
//            else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
//
//                
//                cell.labelFetched.text = data[indexPath.row].HeaderName
//                cell.imageHeader.image = data[indexPath.row].image
//                cell.imageLeft.image = data[indexPath.row].leftimage
//
//                
//                return cell
//                
//            }
//            
////            else if indexPath.row > 0 {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
////
////                    cell.labelFetched.text = data[indexPath.row].HeaderName
////                    cell.imageHeader.image = data[indexPath.row].image
////                    cell.imageLeft.image = data[indexPath.row].leftimage
////                return cell
////
////            } else if indexPath.row == 6 {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
////
////                cell.labelFetched.text = data[indexPath.row].HeaderName
////                return cell
////
////            } else if indexPath.row == 7 {
////                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
////
////                cell.labelFetched.text = data[indexPath.row].HeaderName
////                cell.labelFetched.textColor = .red
////                return cell
////
////            }
////
////
////
////            return UITableViewCell()
//            
//           
//       }
//    
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        if indexPath.row == 0 {
//            let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVc")
//            sb.modalPresentationStyle = .fullScreen
//            sb.modalTransitionStyle = .crossDissolve
//                     self.show(sb, sender: nil)
//        }
//        
//        
//        else if indexPath.row == 1 {
//            
//            self.MyAccountView.isHidden = true
//            self.MyOrderView.isHidden = false
//            self.myCoponView.isHidden = true
//            
//        }
//        
//        
//        else if indexPath.row == 2 {
//           
//            self.MyAccountView.isHidden = false
//            self.MyOrderView.isHidden = true
//            self.myCoponView.isHidden = true
//
//        }
//        
//        else if indexPath.row == 3 {
//            
//            self.MyAccountView.isHidden = true
//            self.MyOrderView.isHidden = true
//            self.myCoponView.isHidden = false
//
//        }
//             
//    }
//    
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0 {
//          
//            
//            return self.tableView.bounds.size.height / 3.5
//        }else {
//            
//            return self.tableView.bounds.size.height / 15
//        }
//    }
//
//  
//
//}
