//
//  Extensions+NotificationCenter.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/7/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation
extension Notification.Name {
    
    //for picker view
    static let SaveCity = Notification.Name("City")
 
}
