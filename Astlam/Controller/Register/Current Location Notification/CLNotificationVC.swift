//
//  CLNotificationVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/16/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class CLNotificationVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                     
               self.navigationController?.navigationBar.shadowImage = UIImage()

    }
    
    @IBAction func meniTapped(_ sender: Any) {
    }
    
    @IBAction func cancleTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func notificationTapped(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OrderPriceVC")
                    vc.modalPresentationStyle = .fullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    show(vc, sender: self)
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
    }
    
}
