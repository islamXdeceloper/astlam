//
//  CurrentLocationVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/6/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
enum Language: String {

case ar = "Arabic"


}
class CurrentLocationVC: BaseViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var refreshBtn: UIButton!
    @IBOutlet weak var notifyBtn: UIButton!
    
    
    let manager = CLLocationManager()
    


    
    
    @IBOutlet weak var myLocation: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
    
        addSlideMenuButton()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                     
        self.navigationController?.navigationBar.shadowImage = UIImage()
        NSLocale.preferredLanguages[0] as String
      let langStr = Locale.current.languageCode
        Locale.characterDirection(forLanguage: "ar_SA")
        Locale.current.currencyCode == "ar_SA"
        print("the code is\(langStr!)")
    }
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
       let langCultureCode: String = "ar_SA"
        let defaults = UserDefaults.standard
        defaults.set([langCultureCode], forKey: "AppleLanguages")
        defaults.synchronize()
    }

    
    func getAddress(lat: Double, lng: Double, handler: @escaping  (String) -> Void) -> Void
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: Double(lat), longitude: Double(lng))
        //selectedLat and selectedLon are double values set by the app in a previous process
        if #available(iOS 11.0, *) {
            geoCoder.reverseGeocodeLocation(location, preferredLocale: Locale.init(identifier: "ar_SA"), completionHandler: { (placemarks, error) -> Void in
                // Place details
                var placeMark: CLPlacemark?
                placeMark = placemarks?[0]
                // Location name
                if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                    address += locationName + ", "
                }
                // Street address
                if let street =
                    placeMark?.addressDictionary?["Thoroughfare"] as? String {
                    address += street + ", "
                }
                // City
                if let city = placeMark?.addressDictionary?["City"] as? String {
                    address += city + ", "

                }
                // Passing address back
                handler(address)
            })
        } else {
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        manager.desiredAccuracy = kCLLocationAccuracyBest //battary
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            manager.startUpdatingLocation()
            
            render(location)
        }
    }
    
    
    func render(_ location:CLLocation) {
        let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let lng = self.manager.location!.coordinate.longitude
        let att = self.manager.location!.coordinate.latitude
            self.getAddress(lat: att, lng: lng) { (address) in
                    
            print(lng)
            print(att)
                self.myLocation.text = address
        }
  
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = coordinate

        mapView.addAnnotation(pin)

        
    }
   
  
    
    @IBAction func SendOrder(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OrderListVC")
               vc.modalPresentationStyle = .fullScreen
               vc.modalTransitionStyle = .crossDissolve
               show(vc, sender: self)
        
    }
    
    @IBAction func NotificationRed(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CLNotificationVC")
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .crossDissolve
                show(vc, sender: self)
        
    }
    
    @IBAction func Refresh(_ sender: Any) {
    }
    
  
}

extension Locale {

    static var enLocale: Locale {

        return Locale(identifier: "ar-AR")
    } // to use in **currentLanguage** to get the localizedString in English

    static var currentLanguage: Language? {

        guard let code = preferredLanguages.first?.components(separatedBy: "-").last else {

            print("could not detect language code")

            return nil
        }

        guard let rawValue = enLocale.localizedString(forLanguageCode: code) else {

            print("could not localize language code")

            return nil
        }

        guard let language = Language(rawValue: rawValue) else {

            print("could not init language from raw value")

            return nil
        }
        print("language: \(code)-\(rawValue)")

        return language
    }
}
