//
//  CityViewController.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/6/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {

 
    @IBOutlet weak var tableView: UITableView!
    
    
    var selectName = ""
      var selectNumber = 0
    let locationName = ["مندوب العمرة","مندوب العزيزية","مندوب النزهة","مندوب الشوقية"]
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.post(name: .SaveCity, object: self)


    }

}


extension CityViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCollectionViewCell", for: indexPath) as! CityCollectionViewCell
        
        cell.mandopLabel.text = locationName[indexPath.row]
        cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectName = locationName[indexPath.row]
                selectNumber = indexPath.row + 1

                self.dismiss(animated: true, completion: nil)
                
                
                NotificationCenter.default.post(name: .SaveCity, object: self)

    }
    
    
  
    
 
    
}
