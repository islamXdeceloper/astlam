//
//  CustomAlertVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/16/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import Cosmos
import TinyConstraints

class CustomAlertVC: UIViewController {
        lazy var cosmosView: CosmosView = {
            var view = CosmosView()
    //        view.settings.updateOnTouch = false
            view.settings.filledImage = UIImage(named: "StatFill")?.withRenderingMode(.alwaysOriginal)
            view.settings.emptyImage = UIImage(named: "startEmpty")?.withRenderingMode(.alwaysOriginal)
            view.settings.totalStars = 5
            view.settings.starSize = 45
            view.settings.starMargin = 5
            view.settings.fillMode = .full
            return view
        }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        view.backgroundColor = .clear
             
             view.addSubview(cosmosView)
             cosmosView.centerInSuperview()
             
             cosmosView.didTouchCosmos = { rating in
                 print("Rated: \(rating)")
             }
    }
    
    @IBAction func SendRating(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func NotNow(_ sender: Any) {
    }
    

}
